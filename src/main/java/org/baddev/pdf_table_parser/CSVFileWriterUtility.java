package org.baddev.pdf_table_parser;

import com.agileengine.pdf.model.ContentBox;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collector;

import static java.util.stream.Collectors.*;

public final class CSVFileWriterUtility {

    private CSVFileWriterUtility() {
    }

    /**
     * Writes {@param cbEntries} into the file under the specified {@param path} according to RFC4180
     *
     * @param cbEntries entries list to write, not null
     * @param path      path, not null
     * @throws IOException occurs if the path presented as a directory rather than
     *                  a regular file, does not exist but cannot be created,
     *                  or cannot be opened for any other reason.
     */
    public static void write(Collection<ContentBox> cbEntries, Path path) throws IOException {
        if (path == null) throw new IllegalArgumentException("path must be a non-null value");
        if (cbEntries == null) throw new IllegalArgumentException("cbEntries must be a non-null value");
        if (cbEntries.isEmpty()) return;

        TreeMap<Double, List<ContentBox>> rowNumToContentBoxesMap = cbEntries.stream()
                .collect(groupingBy(
                        contentBox -> contentBox.getBounds().getY(),
                        TreeMap::new,
                        toSortedList()
                ));

        try (FileWriter fileWriter = new FileWriter(path.toFile());
             CSVPrinter printer = new CSVPrinter(fileWriter, CSVFormat.RFC4180)) {
            for (Map.Entry<Double, List<ContentBox>> entry : rowNumToContentBoxesMap.entrySet()) {
                printer.printRecord(entry.getValue().stream().map(ContentBox::getText).toArray());
            }
        }
    }

    private static Collector<ContentBox, ?, List<ContentBox>> toSortedList() {
        return collectingAndThen(toList(),
                l -> l.stream().sorted(contentBoxComparator()).collect(toList()));
    }

    private static Comparator<ContentBox> contentBoxComparator() {
        return Comparator.comparingDouble((ToDoubleFunction<ContentBox>) value -> value.getBounds().getY())
                .thenComparingDouble(value -> value.getBounds().getX());
    }

}
