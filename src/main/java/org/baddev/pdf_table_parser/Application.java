package org.baddev.pdf_table_parser;

import com.agileengine.pdf.model.ContentBox;
import com.agileengine.pdf.utility.PdfFileReaderUtility;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Application {

    private Application() {
    }

    public static void main(String[] args) {
        parseCmd(args).ifPresent(cmd -> {
            writeOutput(cmd, parseInput(cmd));
        });
    }

    private static void writeOutput(CommandLine cmd, Collection<ContentBox> contentBoxes) {
        String outOpt = cmd.getOptionValue("out");
        try {
            System.out.println(String.format("Writing output to [%s]...", outOpt));
            CSVFileWriterUtility.write(contentBoxes, Paths.get(outOpt));
            System.out.println("Done!");
        } catch (IOException e) {
            System.out.println(String.format("Failed to write output csv file. %s", e.getMessage()));
            System.exit(2);
        }
    }

    private static Collection<ContentBox> parseInput(CommandLine cmd) {
        List<ContentBox> contentBoxes = Collections.emptyList();
        String inOpt = cmd.getOptionValue("in");
        try (InputStream in = Files.newInputStream(Paths.get(inOpt))) {
            System.out.println(String.format("Parsing input file [%s]...", inOpt));
            contentBoxes = PdfFileReaderUtility.readFile(in);
        } catch (IOException e) {
            System.out.println(String.format("Failed to parse input pdf file. %s", e.getMessage()));
            System.exit(1);
        }
        return contentBoxes;
    }

    private static Optional<CommandLine> parseCmd(String[] args) {
        CommandLine cmd = null;
        try {
            cmd = new DefaultParser().parse(createOptions(), args);
        } catch (ParseException e) {
            System.out.println("Failed to parse arguments. " + e.getMessage());
        }
        return Optional.ofNullable(cmd);
    }

    private static Options createOptions() {
        return new Options()
                .addOption(Option.builder("in")
                        .longOpt("input")
                        .hasArg()
                        .desc("Path to input pdf file")
                        .required()
                        .build())
                .addOption(Option.builder("out")
                        .longOpt("output")
                        .hasArg()
                        .desc("Path to output csv file")
                        .required()
                        .build());
    }

}
