package org.baddev.pdf_table_parser;

import com.agileengine.pdf.model.ContentBox;
import com.agileengine.pdf.utility.PdfFileReaderUtility;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(BlockJUnit4ClassRunner.class)
public class ApplicationIT {

    private static final String SAMPLE1 = "file1_formatted_datatable";
    private static final String SAMPLE2 = "file2_ha_empty_datatable";
    private static final String SAMPLE3 = "file3_va_datatable";

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void sample1() throws IOException {
        runSampleAndCheck(SAMPLE1);
    }

    @Test
    public void sample2() throws IOException {
        runSampleAndCheck(SAMPLE2);
    }

    @Test
    public void sample3() throws IOException {
        runSampleAndCheck(SAMPLE3);
    }

    private void runSampleAndCheck(String sampleName) throws IOException {
        Application.main(new String[]{
                "-in",
                inPath(sampleName),
                "-out",
                outPath(sampleName)
        });
        checkResult(sampleName);
    }

    private void checkResult(String sampleName) throws IOException {
        List<ContentBox> contentBoxes = PdfFileReaderUtility.readFile(Files.newInputStream(Paths.get(inPath(sampleName))));
        Reader in = new FileReader(outPath(sampleName));
        Iterable<CSVRecord> records = CSVFormat.RFC4180.parse(in);
        List<String> expected = new ArrayList<>();
        records.forEach(r -> {
            r.forEach(expected::add);
        });
        Assert.assertEquals(expected, contentBoxes.stream().map(ContentBox::getText).collect(Collectors.toList()));
    }

    private String outPath(String sampleName) {
        return tempFolder.getRoot().toPath().resolve(sampleName + ".csv").toString();
    }

    private String inPath(String sampleName) {
        try {
            return new File(ApplicationIT.class.getResource("samples/" + sampleName + ".pdf").toURI()).toPath().toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Malformed URI", e);
        }
    }

}
