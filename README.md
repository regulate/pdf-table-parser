# pdf-table-parser

This is a simple CLI application in Java that is able to parse input PDF file, extract table-like formatted data and produce a CSV file as an output.

Run with ```java -jar <pdf-table-parser-x.x.x.jar> -in <pdfInput> -out <csvOutput>```